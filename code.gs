/* TEST */

function main() {
  var pattern = 'estratto';
  var titleOnly = true;
  var regex = true;
  try {
    getData(pattern, titleOnly, regex);
  } catch (e){
    Logger.log("Error: " + e.message());
  }
}



function doGet() {
  var html = HtmlService.createTemplateFromFile("page").evaluate();
  return html;  
}

 

function getData(pattern, titleOnly, regex) {
  Logger.log("New data request");
  try {
    if (typeof(pattern) == "undefined" || pattern.length == 0)
      pattern = '*';
    var response;
    if (regex) {
      response = findDocsRegex(pattern, regex);
    } else {  
      response= findDocs(pattern, titleOnly, regex); 
    }
  } catch (e) {
    return "Error: " + e.toString();
  }
  //Logger.log(response);
  return response;
}



function findDocs(pattern, titleOnly, regex) {
  var list = new Array();  
  var result = new String();
  
  var docs= DocsList.find(pattern);  
  
  for (var i=0; i< docs.length; i++){    
    Logger.log("Check " + docs[i].getName());
    if ( (titleOnly && docs[i].getName().toLowerCase().indexOf(pattern) != -1) || !titleOnly ) {
      //var file = new File(docs[i].getName(), Math.round(Number(docs[i].getSize())/1024), docs[i].getUrl());
      var file = new File(docs[i].getName(), docs[i].getSize(), docs[i].getUrl(), docs[i].getParents()[0].getName(), docs[i].getParents()[0].getUrl());
      Logger.log("Added '" + file.name + "' (" + file.folder + ") to results");
      list.push(file);
    }
  }  
  list.sort(dynamicSort('name'));
  
  Logger.log('Found ' + list.length + ' results for pattern: "' + pattern + '"'); 
  return Utilities.jsonStringify(list);
};



function findDocsRegex(pattern, regex){
  var pageSize = DocsList.MAX_RESULT_SIZE;
  var files = null;
  var token = null; 
  var list = new Array();  
  var result = new String();
  do {
    var result = DocsList.getAllFilesForPaging(pageSize, token);
    docs = result.getFiles();
    token = result.getToken();
    for (var i = 0; i < docs.length; i++) {
      //var file = new File(docs[i].getName(), Math.round(Number(docs[i].getSize())/1024), docs[i].getUrl()); 
      if ( testRegex( docs[i].getName().toLowerCase(), pattern) ) {  
        var file = new File(docs[i].getName(), docs[i].getSize(), docs[i].getUrl(), docs[i].getParents()[0].getName(), docs[i].getParents()[0].getUrl()); 
        Logger.log("Added '" + file.name + "' (" + file.folder + ") to results");
        list.push(file);
      }
    }
  } while (docs.length >= pageSize);
      
  list.sort(dynamicSort('name'));
  
  Logger.log('Found ' + list.length + ' results for pattern: "' + pattern + '" using regexp search'); 
  return Utilities.jsonStringify(list);
}



function File (name, size, url, folder, folderUrl) {
  this.name = name;
  this.size = size;
  this.url = url;  
  this.folder = folder;  
  this.folderUrl = folderUrl;  
}
  


function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}



function testRegex(string, pattern) {
  try {
    var p = new RegExp(pattern,'i');
  } catch (e) {
    Logger.log(e);
  }
  return p.test(string);
}